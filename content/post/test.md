---
title: Week 3
subtitle: Maandag 2017-09-18 t/m vrijdag 2017-09-22
date: 2017-09-18
tags: ["week3","presenteren","KillYourDarling","Prototype"]
---

# Maandag
Vandaag hebben wij in de ochtend ons eerste project gepresenteerd voor de gehele klas.
Onze game (Capture the city) werd goed ontvangen door de klas. Wij hebben wel de feedback
opgeschreven zodat wij dit in onze volgende iteratie kunnen gebruiken.

Na de presentaties hebben wij onze eerste "kill your darling" gedaan. Wij hebben ons eerste
idee achter ons gelaten en gaan opnieuw onderzoek doen naar nieuwe thema's etc.

# Dinsdag
Vandaag heb ik het 3e hoorcollege gevolgd over "prototyping" dit hoorcollege was redelijk kort
maar bevatte een hoop nuttige informatie die ik zeker ga gebruiken in mijn komende iteratie.

Ook heb ik een wordweb gemaakt voor ons nieuwe thema, dit was best lastig aangezien we al 
gebonden waren aan een doelgroep.
![themaWW](../img/themaww.JPG)

# woensdag

Vandaag hebben we al onze wordwebben naast elkaar gelegd en hebben we een thema gekozen. 
Het thema waar wij deze keer voor hebben gekozen is: Ontwerpen
Ook hebben we nogmaals unaniem belosten bij de gekozen doelgroep te blijven omdat hier toch nog
enige twijels over waren binnen ons groepje.

Verder hebben wij informatie ontvangen over spelanalyses en heb ik een workshop bijgewoond
over creatieve techneieken. Hier heb ik geleerd om te divigeren en te configeren om zo bij
goede ideeen te komen.


# Donderdag
-

# Vrijdag


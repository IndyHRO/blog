---
title: Reflectie eerste kwartaal
subtitle: Donderdag 2017-10-26
date: 2017-10-26
tags: ["zelfreflectie","1e kwartaal","ontwerpproces"]
---

# reflectie eerste kwartaal

Mijn eerste kwartaal op HRO was voor mij persoonlijk erg wennen. In de loop van dit
kwartaal heb ik mijn nieuwe klas een beetje leren kennen. Ik heb ook kennis gemaakt met
verschillende creatieve technieken, die ik heb leren toepassen tijdens het ontwerpproces.
Ik was ook erg tevreden over de samenwerking tussen mij en mijn team (Team Creative.)
Ik vindt zelf dat ik veel kennis heb opgedaan op het gebied van het ontwerpproces.
Ik heb verschillende manieren van onderzoeken geleerd en gebruikt zoals bijvoorbeeld:
Doelgroep interviewen, woordwebben maken en moodboards maken toen ik hiermee begon dacht ik dat 
dit tijdverspilling was, maar toen ik het steeds vaker ging doen werd het steeds nuttiger.
Ik heb ook diverse termen en technieken geleerd door het bijwonen van hoorcolleges van 
design theory. Persoonlijk vond ik dit een uitdagend kwartaal en ga de kennis die ik in deze tijd
heb opgedaan zeker meenemen naar de komende iteraties. 

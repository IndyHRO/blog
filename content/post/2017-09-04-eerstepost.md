---
title: Week 1
subtitle: Maandag 2017-09-04 t/m vrijdag 2017-09-08
date: 2017-09-04
tags: ["week1","brainstormen","hoorcollege","onderzoeken"]
---
# Maandag
De ochtend hebben we besteed aan het beter leren kennen van ons eigen groepje.
Wij hebben een teamcaptain aangewezen (Kim Sumac de Jong) en gezamelijke teamafspraken
gemaakt.
Vervolgens hebben we een brainstormsessie gehouden voor ons project,
Hier kwamen kwam niet veel uit behalve een standaard speurtocht spel.

Wij hebben dit idee geschrapt en hadden een pauze ingelast.
Tot onze grote verbazing, had iedereen na deze pauze allemaal frisse en 
interessante ideeën. Wij hebben toen al onze ideeën naast elkaar gelegt en
de beste uitgekozen.

# Dinsdag
Vandaag heb ik mijn eerste design theory hoorcollege gevolgd, deze ging over het 
ontwerpproces. Deze was erg informatief en ga deze informatie zeker gebruiken in
de komende dagen.

# woensdag

Ik heb voor ons onderzoek een interview opgesteld voor onze gekozen doelgroep.
Dit interview hebben wij vervolgens in onze pauze afgenomen bij 5 bouwkunde studenten.
Dit gaf ons iets meer inzicht in de door ons gekozen doelgroep.

# Donderdag
In de ochtend hadden wij ons eerste 3e leermoment ingelast, in dit moment namen wij de tijd
om te kijken naar onze taakverdeling en onze deadlines. Verder heb ik voor mijn persoonlijke
onderzoek naar een thema een wordweb gemaakt:

![Eerste wordweb](../img/eersteww.JPG)

Later op de dag heb ik mijn eerste werkcollege gevolgd, wij moesten met zo min mogelijk tekst
een ontwerpproces laten zien voor het door ons gekozen project.

![Onze poster](../img/werkcollegeposter.JPG)
